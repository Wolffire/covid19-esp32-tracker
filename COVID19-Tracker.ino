/*
 *  Application note: Covid 19 tracker for AZ-Touch and ESP32 DEV KIT 3
 *  Version 1.0
 *  Copyright (C) 2020  Hartmut Wendt  www.zihatec.de
 *  Version 2.0.3
 *  Copyright (C) 2020  Heiko "Wolffire" Stangel https://woelfe.info
 *  
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
ILI9341_BLACK        0x0000     0,   0,   0
ILI9341_NAVY         0x000F     0,   0, 123
ILI9341_DARKGREEN    0x03E0     0, 125,   0
ILI9341_DARKCYAN     0x03EF     0, 125, 123
ILI9341_MAROON       0x7800   123,   0,   0
ILI9341_PURPLE       0x780F   123,   0, 123
ILI9341_OLIVE        0x7BE0   123, 125,   0
ILI9341_LIGHTGREY    0xC618   198, 195, 198
ILI9341_DARKGREY     0x7BEF   123, 125, 123
ILI9341_BLUE         0x001F     0,   0, 255
ILI9341_GREEN        0x07E0     0, 255,   0
ILI9341_CYAN         0x07FF     0, 255, 255
ILI9341_RED          0xF800   255,   0,   0
ILI9341_MAGENTA      0xF81F   255,   0, 255
ILI9341_YELLOW       0xFFE0   255, 255,   0
ILI9341_WHITE        0xFFFF   255, 255, 255
ILI9341_ORANGE       0xFD20   255, 165,   0
ILI9341_GREENYELLOW  0xAFE5   173, 255,  41
ILI9341_PINK         0xFC18   255, 130, 198
*/

/*______Import Libraries_______*/
#include <Arduino.h>
#include <SPI.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <ArduinoHttpClient.h>
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
/*______End of Libraries_______*/

/*__Pin definitions for the Arduino MKR__*/
#define TFT_CS    5
#define TFT_DC    4
#define TFT_MOSI  23
#define TFT_CLK   18
#define TFT_RST   22
#define TFT_MISO  19
#define TFT_LED   15

#define HAVE_TOUCHPAD
#define TOUCH_CS  14
#define TOUCH_IRQ 2
/*_______End of definitions______*/

/*____Calibrate Touchscreen_____*/
#define MINPRESSURE 10 // minimum required force for touch event
#define TS_MINX 370
#define TS_MINY 470
#define TS_MAXX 3700
#define TS_MAXY 3600
/*______End of Calibration______*/

/*____Wifi _____________________*/
///////please enter your sensitive data in the Secret tab/arduino_secrets.h
#define WIFI_SSID "" // Enter your SSID here
#define WIFI_PASS "" // Enter your WiFi password here
// Number of milliseconds to wait without receiving any data before we give up
const int kNetworkTimeout = 60 * 1000;
// Number of milliseconds to wait if no data is available before trying again
const int kNetworkDelay = 2000;
/*______End of Wifi______*/


/* RAW Data to Serial Port */
//#define RAWDATA  // search parsing problems
/* End of RAW Data */



// Time delay loading (min):
const int zeitLaden = 15;

// Change display (sec):
const int zeitWechsel = 15;


int status = WL_IDLE_STATUS;
String totalCases;
String newCases;
String totalDeaths;
String newDeaths;
String totalRecovered;
String activeCases;

unsigned long zeit1, zeit2;
String daten[10][8] = {{},{}};

WiFiClientSecure client;
HttpClient http(client, "www.worldometers.info", 443);

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_RST);


void setup()
{
  // Initialize serial and wait for port to open:
  Serial.begin(115200);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // init GPIOs
  pinMode(TFT_LED, OUTPUT); // define as output for backlight control

  // initialize the TFT
  Serial.println("Init TFT ...");
  tft.begin();
  tft.setRotation(3);             // landscape mode  
  tft.fillScreen(ILI9341_BLACK);  // clear screen 

  tft.setCursor(55, 115);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(2);
  tft.print("Verbinde mit WLAN");
  digitalWrite(TFT_LED, LOW);     // LOW to turn backlight on; 


  // Set WiFi to station mode and disconnect from an AP if it was Previously
  // connected
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  WiFi.mode(WIFI_STA);

  // Attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  int esp_reboot = 0;
  int wifi_retry = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    wifi_retry++;
    esp_reboot++;
    Serial.print(".");
    delay(1000);
    if(wifi_retry >= 3)
    {
      wifi_retry = 0;
      Serial.println("\nWiFi reset");
      WiFi.disconnect();
      WiFi.mode(WIFI_OFF);
      WiFi.mode(WIFI_STA);
      WiFi.begin(WIFI_SSID, WIFI_PASS);
    }
    if(esp_reboot >= 30)
    {
      Serial.println("\nReboot");
      ESP.restart();
    }
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);

  tft.fillScreen(ILI9341_BLACK);
  tft.setCursor(45, 50);
  tft.print("Verbunden mit WLAN:");
  tft.setCursor(45, 80);
  tft.print(WIFI_SSID);
  tft.setCursor(45, 150);
  tft.print("IP Adresse:");
  tft.setCursor(45, 180);
  tft.print(ip);

  delay(5000);

  // Your countries
  daten[0][0] = "World";
  daten[1][0] = "Germany";
}

void loop()
{
  for (int i=0; i<10; i++)
  {
    if (daten[i][0].length() == 0)
    {
      break;
    }

    draw_country_screen(daten[i][0]);

    if (daten[i][1].toInt() == 0 || millis()-daten[i][1].toInt() > (zeitLaden * 60 * 1000))
    {
      daten[i][1] = millis();
      check_country(daten[i][0]);
    }
    else
    {
      delay(zeitWechsel * 1000);
    }
  }
}


void draw_country_screen(String sCountry)
{
  int i;
  for (i=0; i<10; i++)
  {
    if (daten[i][0] == sCountry)
    {
      break;
    }
  }
  
  tft.fillScreen(ILI9341_BLACK); // clear screen

  // headline
  tft.setCursor(5, 10);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  tft.print("> " + daten[i][0]);

  // totalCases
  tft.setCursor(5, 50);
  tft.setTextColor(ILI9341_RED);
  tft.print("Gemeldet:");
  tft.setCursor(190, 50);
  tft.print(daten[i][2]);

  // activeCases
  tft.setCursor(5, 90);
  tft.setTextColor(ILI9341_ORANGE);
  tft.print("Aktiv:");
  tft.setCursor(190, 90);
  tft.print(daten[i][7]);

  /*
  // totalRecovered
  tft.setCursor(5, 90);
  tft.setTextColor(ILI9341_ORANGE);
  tft.print("Recovered:");
  tft.setCursor(190, 90);
  tft.print(daten[i][6]);
  */
  
  // newCases
  tft.setCursor(5, 130);
  tft.setTextColor(ILI9341_GREEN);
  tft.print("Neu/24h:");
  tft.setCursor(190, 130);
  tft.print(daten[i][3]);

  // totalDeaths
  tft.setCursor(5, 170);
  tft.setTextColor(ILI9341_LIGHTGREY);
  tft.print("Gestorben:");
  tft.setCursor(190, 170);
  tft.print(daten[i][4]);

  // newDeaths
  tft.setCursor(5, 210);
  tft.setTextColor(ILI9341_MAGENTA);
  tft.print("Tote/24h:");
  tft.setCursor(190, 210);
  tft.print(daten[i][5]);
}

void check_country(String sCountry)
{
  for (int i=0; i<10; i++)
  {
    if (daten[i][0].length() == 0)
    {
      break;
    }
    if (daten[i][0] == sCountry)
    {
      for (int j=2; j<8; j++)
      {
        daten[i][j] = "?";
      }
    }
  }

  int err = 0;
  int readcounter = 0;
  int read_value_step = 0;
  String s1 = "";
  String s2 = "";

  err = http.get("/coronavirus/");
  if (err == 0)
  {
    Serial.println("");
    Serial.println("Starte... Request ok");

    err = http.responseStatusCode();
    if (err >= 0)
    {
      Serial.print("Got status code: ");
      Serial.println(err);

      // Usually you'd check that the response code is 200 or a
      // similar "success" code (200-299) before carrying on,
      // but we'll print out whatever response we get

      // If you are interesting in the response headers, you
      // can read them here:
      //while(http.headerAvailable())
      //{
      //  String headerName = http.readHeaderName();
      //  String headerValue = http.readHeaderValue();
      //}

      Serial.print("Request data for ");
      Serial.println(sCountry);

      // Now we've got to the body, so we can print it out
      unsigned long timeoutStart = millis();
      char c;
      int ok = 0;
      // Whilst we haven't timed out & haven't reached the end of the body
      while ((http.connected() || http.available()) && (!http.endOfBodyReached()) && ((millis() - timeoutStart) < kNetworkTimeout))
      {
        if (http.available())
        {
          int newStart = 0;
          c = http.read();
          readcounter++;
          if (readcounter > 9000 && readcounter < 13000 && sCountry == "World") // Oberen Teil parsen
          {
            s1 = s1 + c;
          }
          else if (readcounter > 66000 && readcounter < 86000 && sCountry != "World") // Tabelle (erste 15-20 grob) parsen
          {
            s1 = s1 + c;
          }
          else if (readcounter > 85000) // Parsen abbrechen
          {
            http.stop();
          }
        }
      }

      #ifdef RAWDATA
        Serial.println(s1);
      #endif
      
      int i;
      for (i=0; i<10; i++)
      {
        if (daten[i][0] == sCountry)
        {
          break;
        }
      }

      if (daten[i][0] == "World")
      {
        int place = s1.indexOf("Coronavirus Cases:");
        if (place != -1)
        {
          s1 = s1.substring(place);
          s1 = s1.substring(s1.indexOf("#aaa") + 6);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][2] = s2.toInt();
          Serial.print("Total Cases: ");
          Serial.println(daten[i][2]);
        }
        
        place = s1.indexOf("Deaths:");
        if (place != -1)
        {
          s1 = s1.substring(place);
          s1 = s1.substring(s1.indexOf("<span>") + 6);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][4] = s2.toInt();
          Serial.print("Total Deaths: ");
          Serial.println(daten[i][4]);
        }
        
        place = s1.indexOf("Recovered:");
        if (place != -1)
        {
          s1 = s1.substring(place);
          s1 = s1.substring(s1.indexOf("<span>") + 6);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][6] = s2.toInt();
          Serial.print("Total Recovered: ");
          Serial.println(daten[i][6]);
        }
        
        place = s1.indexOf("Active Cases");
        if (place != -1)
        {
          s1 = s1.substring(place);
          s1 = s1.substring(s1.indexOf("-main") + 7);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][7] = s2.toInt();
          Serial.print("Active Cases: ");
          Serial.println(daten[i][7]);
        }
      }
      else
      {
        int place = s1.indexOf(sCountry);
        if (place != -1)
        {
          s1 = s1.substring(place);
          s1 = s1.substring(s1.indexOf("\">") + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][2] = s2.toInt();
          Serial.print("Total Cases: ");
          Serial.println(daten[i][2]);
        }

        place = s1.indexOf("\">");
        if (place != -1)
        {
          s1 = s1.substring(place + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][3] = s2.toInt();
          Serial.print("New Cases: ");
          Serial.println(daten[i][3]);
        }
        
        place = s1.indexOf("\">");
        if (place != -1)
        {
          s1 = s1.substring(place + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][4] = s2.toInt();
          Serial.print("Total Deaths: ");
          Serial.println(daten[i][4]);
        }
        
        place = s1.indexOf("\">");
        if (place != -1)
        {
          s1 = s1.substring(place + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][5] = s2.toInt();
          Serial.print("New Deaths: ");
          Serial.println(daten[i][5]);
        }
        
        place = s1.indexOf("\">");
        if (place != -1)
        {
          s1 = s1.substring(place + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][6] = s2.toInt();
          Serial.print("Total Recovered: ");
          Serial.println(daten[i][6]);
        }
        
        place = s1.indexOf("\">");
        if (place != -1)
        {
          s1 = s1.substring(place + 2);
          s2 = s1.substring(0, (s1.indexOf("</")));
          s2.replace(",", "");
          daten[i][7] = s2.toInt();
          Serial.print("Active Cases: ");
          Serial.println(daten[i][7]);
        }
      }
      return;
    }
  }
  else
  {
    Serial.print("Connect failed: ");
    Serial.println(err);
  }
  http.stop();
}


void printWiFiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}