# COVID19-ESP32-Tracker

This little tracker will help you to be up to date about the Corona virus outbreak and the situation in your country. The display shows alternating the current data of different countries of your choice.

The data is collected by the website https://www.worldometers.info/coronavirus/

A picture of the new interface:
![My new interface](https://gitlab.com/Wolffire/covid19-esp32-tracker/-/raw/master/COVID19-ESP32-Tracker.jpg)


## Inspired by

- https://www.az-delivery.de/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/covid19-tracker-esp32?goal=0_569b1a8f94-38b91429a2-18827199&mc_cid=38b91429a2&mc_eid=27c23d3818
- https://github.com/HWHardsoft/COVID19-Tracker-ESP32


## Changes

Last Changes:

- I only parse the start page
- The new positions for parsing adjusted. (again)



## Hardware 

I've used our AZ-Touch kit for ESP32 as hardware plattform. This kit comes with a 2.4 inch tft touchscreen, which will be used for the data output.


## WiFi settings

Enter your WiFi SSID & password in the fields in the WiFi section: 

- Line 79: #define WIFI_SSID "YOUR-SSID"
- Line 80: #define WIFI_PASS "YOUR-PW"


## Country settings

You can change/add/delete the countries in the main loop of the program according your interests.

- Line 195: daten[0][0] = "World";
- Line 196: daten[1][0] = "Germany";
- further lines until
- daten[9][0] = "XXX"


## Timing settings

- Line 95: Time delay loading (min)
- Line 98: Change display (sec)


# License

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.